Author: Haojun Su <suyhy010@mymail.unisa.edu.au>
Date:   Sun Oct 6 18:36:26 2019 +1030

    Update

commit 1870fe3914c244fb02feafeb67f2f1718b7c235d
Author: Su <suyhy010@mymail.unisa.edu.au>
Date:   Sun Oct 6 00:02:11 2019 +0930

    Update

commit c2531e39bd2b36481960f1d576ee91ed809c1165
Author: Su <suyhy010@mymail.unisa.edu.au>
Date:   Sat Oct 5 20:16:32 2019 +0930

    Update

commit f06d62d5def2c5e0f9306f2fa27741d1f192348d
Author: Haojun SU <suyhy010@mymail.unisa.edu.au>
Date:   Fri Sep 27 04:09:38 2019 +0000

    Update CashRegister.java

commit f4a4487a7e39ca1cdd1aaefaddd71496341817fe
Author: Haojun SU <suyhy010@mymail.unisa.edu.au>
Date:   Thu Sep 26 06:48:30 2019 +0000

    Update CashRegister.java

commit 52aeff644eb73ed4259643fa9ad8ae91107fd9f8
Author: Haojun SU <suyhy010@mymail.unisa.edu.au>
Date:   Thu Sep 26 06:07:39 2019 +0000

    Update CashRegister.java

commit e7cb444e3c3bfe3c2c60dbcd53c6e0c0d8015153
Author: Haojun SU <suyhy010@mymail.unisa.edu.au>
Date:   Thu Sep 26 05:59:53 2019 +0000

    Update CashRegister.java

commit 4550d110e26982d7d836f27a7005cb775f8c2bae
Author: Ivan Lee <ivan.lee@unisa.edu.au>
Date:   Thu Sep 5 00:08:36 2019 +0000

    Copy the due date from the course website to specification.md

commit 10ddb5fa404ea8a1f1b0c4521b31c312127a6024
Author: Ivan Lee <ivan.lee@unisa.edu.au>
Date:   Wed Sep 4 15:37:09 2019 +0930

    update assignment specification to v2.0

commit 52fec618e66a4d145a53513540be85b1ed329ebe
Author: Ivan Lee <ivan.lee@unisa.edu.au>
Date:   Wed Aug 14 15:39:10 2019 +0930

    fixed git clone path

commit 109958d69f27edcd572face746f6cc3d4cd8ac0f
Author: Ivan Lee <ivan.lee@unisa.edu.au>
Date:   Wed Aug 14 15:35:18 2019 +0930

    Initial files
