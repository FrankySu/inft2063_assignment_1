import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.Locale;

public class CashRegister {
	public static void main(String[] args) {
		String s, c, c1, name, receipt, pf;
		double balance;
		double cash;
		double cost = 0, gn;
		double d, f, Total_cost = 0, Item_cost = 0;
		String[] Item_Name = new String[99];
		double[] Item_Number = new double[99];
		double[] Item_Cost = new double[99];
		int[] Index = new int[99];
		int i = 0, Index_Number;

		Scanner in = new Scanner(System.in);

		System.out.print("Please enter cash register's float: $");
		while (true) {
			try {
				s = in.nextLine();
				balance = Double.parseDouble(s);
				break;
			} catch (Exception e) {
				System.out.println("Please, enter number: ");
			}
		}

		System.out.print("Would you want to start? (y/n) ");
		c1 = in.next().toLowerCase();

		while (!c1.equals("y") && !c1.equals("n")) {
			System.out.print("Please, enter 'y' or 'n':");
			c1 = in.next().toLowerCase();
		}

		if (c1.equalsIgnoreCase("y")) {
			System.out.println("Welcome to use our Cash Register, we are very happy to sever for you!");

			while (true) {
				while(true) {
					System.out.print("Please enter the item's name:");
					name = in.next();

					System.out.print("Please enter the " + name + " cost: $");
					while (true) {
						try {
							c = in.next();
							cost = Double.parseDouble(c);
							break;
						} catch (Exception e) {
							System.out.println("Please, enter number: ");
						}
					}

					System.out.print("Please enter How many you get: ");
					while (true) {
						try {
							c = in.next();
							gn = Double.parseDouble(c);
							break;
						} catch (Exception e) {
							System.out.println("Please, enter number: ");
						}
					}
					Item_cost = cost * gn;
					System.out.println("Total cost for " + name + " * " + c + " is: $" + Item_cost);
					
					Index_Number = i + 1;
					Item_Name[i] = name;
					Item_Number[i] = gn;
					Item_Cost[i] = Item_cost;
					Total_cost += Item_cost;
					Index[i] = Index_Number;
					
					System.out.print("Finish? (y/n) ");
					c1 = in.next().toLowerCase();

					while (!c1.equals("y") && !c1.equals("n")) {
						System.out.print("Please, enter 'y' or 'n':");
						c1 = in.next().toLowerCase();
					}
					
					if(c1.equals("n")){
						i++;
						continue;
					}
					else{
						break;
					}
				}
				
				System.out.println("Total cost for your all items are: $" + Total_cost);

				Transaction trans = new Transaction(name, Total_cost);

				System.out.print("Please enter the cash amount tendered: $");
				while (true) {
					try {
						s = in.next();
						cash = Double.parseDouble(s);
						break;
					} catch (Exception e) {
						System.out.println("Please, enter number: ");
					}
				}

				d = cash - Total_cost;
				while (d < 0) {
					System.out.println("You should pay more $" + d);
					System.out.println("Please enter the cash amount total tendered: $");
					while (true) {
						try {
							s = in.nextLine();
							cash = Double.parseDouble(s);
							break;
						} catch (Exception e) {
							System.out.println("Please, enter number: ");
						}
					}
					d = cash - Total_cost;
				}
				System.out.println("Amount of change required = $" + d);
				
				System.out.println("Would you like to print the receipt? (y/n)");
				receipt = in.next().toLowerCase();

				while (!receipt.equals("y") && !receipt.equals("n")) {
					System.out.print("Please, enter 'y' or 'n':");
					receipt = in.next().toLowerCase();
				}
				
				if (receipt.equalsIgnoreCase("y")) {
					System.out.println("******************************************************************************");
					System.out.println("|          Item          |           Qty            |          Cost          |");
					System.out.println("|************************|**************************|************************|");
					for(int j = 0; j < Item_Name.length; j++) {
						if(Item_Name[j] != null) {
							s = String.format("|%24s|%26.2f|%24s|", Item_Name[j], Item_Number[j], DecimalFormat.getCurrencyInstance(Locale.US).format(Item_Cost[j]));
							System.out.println(s);
						}
					}
					System.out.println("|****************************************************************************|");
					s = String.format("|%s%63s|", "Total amount:", DecimalFormat.getCurrencyInstance(Locale.US).format((Total_cost)));
					System.out.println(s);
					s = String.format("|%s%71s|", "Cash:", DecimalFormat.getCurrencyInstance(Locale.US).format((cash)));
					System.out.println(s);
					s = String.format("|%s%60s|", "Change required:", DecimalFormat.getCurrencyInstance(Locale.US).format((d)));
					System.out.println(s);
					System.out.println("******************************************************************************");
				}
				
				System.out.println("Please make the choice of your satisfied (1:Exellent  2:Good  3:Not satisfied) : ");
				pf = in.next();

				while (!pf.equals("1") && !pf.equals("2") && !pf.equals("3")) {
					System.out.print("Please, enter '1' or '2' or '3':");
					pf = in.next();
				}
				
				if(pf.equals("1")) {
					System.out.println("Thank you so much!");
				}
				
				if(pf.equals("2")) {
					System.out.println("Thank you, I will do better.");
				}
				
				if(pf.equals("3")) {
					System.out.println("I am so sorry.");
				}
				
				System.out.println("See you, have a good day!");
				break;

			}

		} else
			System.out.println("See you, have a good day!");

		f = balance + Total_cost;
		System.out.println("Balance of the Cash Register: $" + f);
	}

}